# Analytic Distributed DBMS for the project SW-StarLab

We improve analytic DBMS, a special kind of DBMS which stores big data for analytic purposes. 
We are developing �ADDB (Analytic Distributed DBMS)�, an in-memory, distributed version of analytic DBMS based on flash memory storage.
In this project, emerging persistent storage such as NVRAM will also be concerned.

This research was supported by the MSIT(Ministry of Science and ICT), Korea, under the SW Starlab support program(IITP-2017-0-00477) supervised by the IITP(Institute for Information & communications Technology Promotion).